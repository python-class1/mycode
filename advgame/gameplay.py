#!/bin/bash/env python3

ans = input("Would you like to go on an adventure? (yes/no)")

if ans.lower() == "yes":
    ans = input("You wake up disoriented & locked in an unknown room, what would you like to do next? \n1. explore or 2.cry out for help (1/2)")
    if ans == "1":
        print("You get up and start looking around the room, you discover a note on the desk by you and a window that is cracked open slightly")
        ans = input("What do you want to do next: \n 1. read the note or 2. try and escape out of the window (1/2)")
    else:
        print("your cries for help have alerted the bad guys, YOU LOSE!")

else:
    print("That's too bad, maybe next time")
